# Serverless Framework Node with Typescript HTTP API on AWS

This template demonstrates how to make a simple HTTP API with Node.js and Typescript running on AWS Lambda and API Gateway using the Serverless Framework v3.

This template does not include any kind of persistence (database). For more advanced examples, check out the [serverless/examples repository](https://github.com/serverless/examples) which includes Typescript, Mongo, DynamoDB and other examples.

## Setup

Run this command to initialize a new project in a new working directory.

```console
$ yarn
```

## Usage

### Deploy

```console
$ serverless deploy
✔ Service deployed to stack aws-node-http-api-typescript-dev
```

### Invoke the function locally

```console
$ serverless invoke local --function hello
{
    "statusCode": 200,
    "body": "{\n  \"message\": \"Hello from Serverless v3!\",\n  \"input\": \"\"\n}"
}
```

### Invoke the function

```console
$ curl https://xxxxxxxxx.execute-api.us-east-1.amazonaws.com/
{
    "message": "Hello from Serverless v3!",
    ...
}
```
